<?php get_header(); ?>

                <div class="row">
                    <div class="col-md-6 feature-img-col">
                        <div class="col-md-12 feature-home-lg">
                            <a href="#"> </a>
                        </div>
                    </div>
                    <div class="col-md-6 feature-main pb-4">
                        <div class="row feature-tags">
                            <div class="col-md-12">
                                <div class="topic-tag">
                                    <?php _e( 'Innovation', 'euregio' ); ?>
                                </div>
                                <div class="format-tag">
                                    <?php _e( 'Ausstellung', 'euregio' ); ?>
                                </div>
                                <div class="topic-tag">
                                    <?php _e( 'Geschichte', 'euregio' ); ?>
                                </div>
                            </div>
                        </div>
                        <div class="row feature-title ml-1 ">
                            <div>
                                <?php _e( 'Die digitale Retour-Kutsche', 'euregio' ); ?>
                                <?php _e( 'Aus der Welt zurück nach Neustift', 'euregio' ); ?>
                            </div>
                        </div>
                        <div class="row feature-subtitle">
                            <div>
                                <p> <?php _e( 'Kurze Beschreibung zum Projekt und eine wunderbare', 'euregio' ); ?> <?php _e( 'Heiterkeit hat meine ganze Seele eingenommen, gleich', 'euregio' ); ?> <?php _e( 'den süßen Frühlingsmorgen, die mich mit ganzem', 'euregio' ); ?> <?php _e( 'Herzen genieße.', 'euregio' ); ?> </p>
                            </div>
                        </div>
                        <div class="row feature-date ml-1 ">
                            <?php _e( '13. Mar–10. Okt 2021', 'euregio' ); ?>
                        </div>
                        <div class="row feature-location ml-1 ">
                            <?php _e( 'Augustiner Chorherrenstift Neustift', 'euregio' ); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="bg-secondary col-md-12 filter filter-bar">
                        <h4><?php _e( 'Format, Thema, Monat', 'euregio' ); ?></h4>
                    </div>
                </div>
                <?php
                    $post_query_args = array(
                    	'post_type' => 'post',
                    	'posts_per_page' => 200,
                    	'paged' => get_query_var( 'paged' ) ?: 1,
                    	'ignore_sticky_posts' => true,
                    	'order' => 'ASC',
                    	'orderby' => 'date'
                    )
                ?>
                <?php $post_query = new WP_Query( $post_query_args ); ?>
                <?php if ( $post_query->have_posts() ) : ?>
                    <div class="row main-grid">
                        <?php $post_query_item_number = 0; ?>
                        <?php while ( $post_query->have_posts() ) : $post_query->the_post(); ?>
                            <?php if( $post_query_item_number >= 0 && $post_query_item_number <= 200 ) : ?>
                                <?php PG_Helper::rememberShownPost(); ?>
                                <div class="col-md-6 pl-0 pr-0 grid-item<?php if( $post_query_item_number == 0) echo ' first'; ?> <?php echo join( ' ', get_post_class( '' ) ) ?>" id="post-<?php the_ID(); ?>">
                                    <div class="list-img list-img-cont"></div>
                                    <div class=" list-data mt-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="format-tag">
                                                    <?php _e( 'Ausstellung', 'euregio' ); ?>
                                                </div>
                                                <div class="topic-tag">
                                                    <?php _e( 'Innovation', 'euregio' ); ?>
                                                </div>
                                                <div class="topic-tag">
                                                    <?php _e( 'Geschichte', 'euregio' ); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="list-title">
                                                    <?php the_title(); ?>
                                                </div>
                                                <div class="list-date">
                                                    <?php _e( 'Okt-Jun 2021', 'euregio' ); ?>
                                                </div>
                                                <div class="list-location">
                                                    <?php _e( 'Naturmuseum Südtirol', 'euregio' ); ?> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <?php $post_query_item_number++; ?>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                    </div>
                <?php else : ?>
                    <p><?php _e( 'Sorry, no posts matched your criteria.', 'euregio' ); ?></p>
                <?php endif; ?>
                <div class="footer">
</div>                

<?php get_footer(); ?>